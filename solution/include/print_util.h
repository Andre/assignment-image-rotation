#ifndef PRINT_ERROR
#define PRINT_ERROR

#include "bmp.h"
#include "file.h"
#include <inttypes.h>


void print_message(const char* message);
void print_error(const char* string);
#endif
