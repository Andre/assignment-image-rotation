#ifndef BMP_STATUS_FILE
#define BMP_STATUS_FILE

#include "bmp.h"

void check_read_bmp(enum read_status status_code);
void check_write_bmp(enum write_status status_code);

#endif
