#ifndef FILE_H
#define FILE_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>



enum status_file  {
  FILE_OK = 0,
  FILE_OPEN_ERROR,
};

FILE* open_file_for_write(const char* path);
FILE* open_file_for_read(const char* path);
enum status_file get_file_status(FILE* file);

#endif
