#ifndef BMP_ROTATE_RIGHT
#define BMP_ROTATE_RIGHT

#include "image.h"

struct image bmp_rotate_right(struct image image);

#endif
