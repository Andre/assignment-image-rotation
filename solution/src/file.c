#include "file.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>



FILE* open_file_for_write(const char* path) {
    return fopen(path, "wb");
}

FILE* open_file_for_read( const char* path) {
    return fopen(path, "rb");
}

enum status_file get_file_status(FILE* file) {

    if (file == NULL) {
        return FILE_OPEN_ERROR;
    }
    return FILE_OK;
}
