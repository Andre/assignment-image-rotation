
#include "bmp.h"
#include "bmp_rotate_right.h"
#include "bmp_status_file.h"
#include "file.h"
#include "image.h"
#include "print_util.h"
#include <inttypes.h>


#include <stdio.h>
#include <stdlib.h>




int main(int argc, char** argv) {
	
	if (argc != 3) {
		print_error("Input 3 arguments");
		return 0;
	}


	struct image image;
	FILE* file = open_file_for_read(argv[1]);
	enum status_file file_open_status = get_file_status(file);

	if (file_open_status != 0) {
		return 0;
	}

	enum read_status bmp_read_code = from_bmp(file, &image);

	check_read_bmp(bmp_read_code);
	fclose(file);

	if (bmp_read_code != 0) {
		return 0;
	}
	
	struct image rotate_image = bmp_rotate_right(image);

    free(image.data);


    FILE* rotate_file = open_file_for_write(argv[2]);


	file_open_status = get_file_status(rotate_file);

	if (file_open_status != 0) {
		return 0;
	}


	enum write_status write_file_code = to_bmp(rotate_file, &rotate_image);
	free(rotate_image.data);
	check_write_bmp(write_file_code);

	if (write_file_code == 0) {
		return 0;
	}

	fclose(rotate_file);


	print_message("Successfully! Check your new image");

	return 0;
	

}

