#include "bmp_rotate_right.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h> 


static struct image create_image(struct image image) {
    return (struct image) {.width = image.height, .height=image.width};
}


struct image bmp_rotate_right(struct image image) {
    struct image rotate_image = create_image(image);

    rotate_image.data = malloc(sizeof(struct pixel) * rotate_image.height * rotate_image.width);

    for (size_t row = 0; row < image.height; row++) {
        for (size_t column = 0; column < image.width; column++) {
            rotate_image.data[column * image.height + row] = image.data[(image.height - row - 1) * image.width + column];
        }
    }


	return rotate_image;

}
