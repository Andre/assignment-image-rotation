#include <stdio.h>

void print_error(const char* error) {
    fprintf(stderr, "%s\n", error);
}

void print_message(const char* message) {
    printf("%s\n", message);
}
