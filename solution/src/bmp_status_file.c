#include "bmp_status_file.h"
#include "print_util.h"

static const char* bmp_status_check[] = {
	[READ_OK] = "Bmp read ok",
  	[READ_INVALID_SIGNATURE] = "Bmp invalid signature",
  	[READ_INVALID_BITS] = "Bmp invalid bits",
  	[READ_INVALID_HEADER] = "Bmp invalid header"
};

static const char* const bmp_status_write[] = {
  	[WRITE_OK] = "Bmp write normall",
  	[WRITE_ERROR] = "Bmp write error"
};

void check_read_bmp(enum read_status status_code) {

    if (status_code != 0) {
	    print_error(bmp_status_check[status_code]);
    }
    else {
        print_message(bmp_status_check[status_code]);
    }

}

void check_write_bmp(enum write_status status_code) {

    if (status_code != 0) {
	    print_error(bmp_status_write[status_code]);
    }
    else {
        print_message(bmp_status_write[status_code]);
    }
}
