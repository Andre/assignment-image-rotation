#include "bmp.h"
#include "image.h"
#include "print_util.h"
#include <inttypes.h>

#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header
{
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};


static struct bmp_header get_header(struct image const* image) {

    const struct bmp_header bmp = {
		.bfType = 0x4d42,
		.bOffBits = 54,
        .bfReserved = 0,
		.biSize = 40,
        .biWidth = image->width,
		.biHeight = image->height,
		.biPlanes = 1,
		.biBitCount = 24,
		.biCompression = 0,
        .biSizeImage = image->width * image->height * sizeof(struct pixel),
        .bfileSize = image->width * image->height * sizeof(struct pixel) + sizeof(struct bmp_header)
	};

    return bmp;

}





static uint32_t get_padding(uint32_t width){
    return width % 4;
}

enum read_status from_bmp(FILE* in, struct image* image) {

    struct bmp_header bmp;

	if (!fread(&bmp, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_SIGNATURE;
    }

    if (bmp.bfType != 0x4d42) {
        return READ_INVALID_HEADER;
    }

    struct image create_image = {0};

    create_image.height = bmp.biHeight;
    create_image.width = bmp.biWidth;
    create_image.data = malloc(sizeof(struct pixel) * create_image.height * create_image.width);

    const uint8_t padding = get_padding(bmp.biWidth);    


    for (size_t i = 0; i < bmp.biHeight; i++) {
        if (!fread(create_image.data + i * bmp.biWidth, sizeof(struct pixel), bmp.biWidth, in)) {
            return READ_INVALID_SIGNATURE;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }
    }


    *image = create_image;
    return READ_OK;
	
}

enum write_status to_bmp(FILE* out, struct image const* image ) {

	const struct bmp_header bmp = get_header(image);


    if(!fwrite(&bmp, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }
    if (fseek(out, bmp.bOffBits, SEEK_SET) != 0) {
        return WRITE_ERROR;
    }


	const uint8_t padding_elements[] = {0, 0, 0};
    const uint8_t padding = get_padding(bmp.biWidth);    

    for (size_t i = 0; i < image->height; i++) {
        fwrite(image->data + i * image->width, sizeof(struct pixel), image->width, out);
        fwrite(padding_elements, 1, padding, out);
    }



    return WRITE_OK;
}

